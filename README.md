Created repo for testing purposes

Build:
    docker-compose up -d --build

Known issues:
    Контейнеры запускаются под рутом = проблемы с безопасностью

TODO:
1. Programming language: Python
2. OS: Linux/Ubuntu 20.04
3. Git: Gitlab
4. WebServer: Nginx
5. CI/CD: Gitlab CI or Jenkins
6. Containers: Docker
7. Configuration managment: Ansible
8. Container orchestration: Kubernetes
9. Infrastructure provisioning: Terraform
10. Infrastructure monitoring: Prometheus or Grafana
11. Logs: Elastic Stack
