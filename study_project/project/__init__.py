#!/usr/bin/env python
 # -*- coding: utf-8 -*-
from flask import Flask
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func


app = Flask(__name__)
app.config.from_object("project.config.Config")
db = SQLAlchemy(app)

class Text(db.Model):
    __tablename__="texts"
 
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(250))
    timestamp = db.Column(db.DateTime, server_default=func.now())

    def init(self, text):
        self.text = text



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api')
def api():
    return 'test1.'

@app.route('/colors')
def colors():
    return 'test2'