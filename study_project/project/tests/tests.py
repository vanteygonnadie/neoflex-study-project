from flask import url_for
from ..app import *

with app.test_request_context():
    print(url_for('index'))
    print(url_for('colors'))
    print(url_for('api'))